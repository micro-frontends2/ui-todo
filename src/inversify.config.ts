import { Container } from 'inversify'
import { TaskRepositoryImpl } from './app/features/todo/data/repositories/taskRepositoryImpl'
import type { ITaskRepository } from './app/features/todo/domain/repositories/task.repository'

const container = new Container()

container
  .bind<ITaskRepository>('ITaskRepository')
  .to(TaskRepositoryImpl)
  .inSingletonScope()

export default container
