import { ITaskRepository } from '../repositories/task.repository'

export class ToggleStateTaskUseCase {
  private readonly _repository: ITaskRepository

  constructor(repository: ITaskRepository) {
    this._repository = repository
  }

  public async execute(taskId: number): Promise<void> {
    await this._repository.toggleState(taskId)
  }
}
