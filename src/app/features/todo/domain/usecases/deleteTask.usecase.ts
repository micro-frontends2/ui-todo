import { ITaskRepository } from '../repositories/task.repository'

export class DeleteTaskUseCase {
  private _repository: ITaskRepository

  constructor(repository: ITaskRepository) {
    this._repository = repository
  }

  public async execute(taskId: number): Promise<void> {
    await this._repository.remove(taskId)
  }
}
