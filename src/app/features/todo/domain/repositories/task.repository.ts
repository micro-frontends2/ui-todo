import { Task } from '../entities/task.entity'

export interface ITaskRepository {
  getAll(): Promise<Task[]>
  add(task: Task): Promise<Task>
  remove(taskId: number): Promise<void>
  toggleState(taskId: number): Promise<void>
}
