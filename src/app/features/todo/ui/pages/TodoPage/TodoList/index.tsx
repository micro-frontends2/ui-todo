import container from '../../../../../../../inversify.config'
import { Task } from '../../../../domain/entities/task.entity'
import { DeleteTaskUseCase } from '../../../../domain/usecases/deleteTask.usecase'
import { ToggleStateTaskUseCase } from '../../../../domain/usecases/toggleStateTask.usecase'
import { useAppDispatch } from '../../../hooks/reduxHooks'
import { useTodoList } from './hooks/useTodoList'
import { removeTask, toggleStateTask } from '../../../redux/taskSlice'
import TodoListItem from '../TodoListItem'
import { GetAllTasksUseCase } from '../../../../domain/usecases/getAllTasks.usecase'
import { ITaskRepository } from '../../../../domain/repositories/task.repository'

function TodoList() {
  const repository: ITaskRepository = container.get('ITaskRepository')
  const getAllTasksUseCase = new GetAllTasksUseCase(repository)
  const toggleStateTaskUseCase = new ToggleStateTaskUseCase(repository)
  const deleteTaskUseCase = new DeleteTaskUseCase(repository)

  const { todos, loading, error } = useTodoList({ useCase: getAllTasksUseCase })
  const dispatch = useAppDispatch()

  const handleToggleState = async (id: number) => {
    await toggleStateTaskUseCase.execute(id).then(() => {
      dispatch(toggleStateTask(id))
    })
  }

  const handleDelete = async (id: number) => {
    await deleteTaskUseCase.execute(id).then(() => {
      dispatch(removeTask(id))
    })
  }

  return (
    <>
      {loading && <div>Loading...</div>}
      {error && <div>{error}</div>}
      {todos.map((todo: Task) => (
        <TodoListItem
          key={todo.id}
          taskId={todo.id}
          title={todo.title}
          defaultValue={todo.completed}
          onToggleState={handleToggleState}
          onDelete={handleDelete}
        />
      ))}
    </>
  )
}

export default TodoList
