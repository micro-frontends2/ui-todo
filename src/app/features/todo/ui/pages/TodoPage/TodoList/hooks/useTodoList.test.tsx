import { renderHook } from '@testing-library/react-hooks'
import { ReactNode } from 'react'
import { Provider } from 'react-redux'
import container from '../../../../../../../../inversify.config'
import { store } from '../../../../../../../store'
import { ITaskRepository } from '../../../../../domain/repositories/task.repository'
import { GetAllTasksUseCase } from '../../../../../domain/usecases/getAllTasks.usecase'
import { useTodoList } from './useTodoList'

type ReduxProviderProps = {
  children: ReactNode
  reduxStore: any
}

const ReduxProvider = ({ children, reduxStore }: ReduxProviderProps) => (
  <Provider store={reduxStore}>{children}</Provider>
)

describe('useTodoList', () => {
  it('should validate array length', async () => {
    const repository: ITaskRepository = container.get('ITaskRepository')
    const useCase = new GetAllTasksUseCase(repository)

    const wrapper = ({ children }) => (
      <ReduxProvider reduxStore={store}>{children}</ReduxProvider>
    )
    const { result, waitForNextUpdate } = renderHook(
      () => useTodoList({ useCase }),
      {
        wrapper,
      }
    )
    expect(result.current.loading).toBeTruthy()

    await waitForNextUpdate({ timeout: 5000 })

    expect(result.current.loading).toBeFalsy()
    console.log(result.current.todos.length)

    expect(result.current.todos).toHaveLength(200)
  })
})
