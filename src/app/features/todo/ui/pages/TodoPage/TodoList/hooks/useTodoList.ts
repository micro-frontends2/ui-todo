import { useEffect, useState } from 'react'
import { GetAllTasksUseCase } from '../../../../../domain/usecases/getAllTasks.usecase'
import { setList } from '../../../../redux/taskSlice'
import { useAppDispatch, useAppSelector } from '../../../../hooks/reduxHooks'

type Props = {
  useCase: GetAllTasksUseCase
}

export const useTodoList = ({ useCase }: Props) => {
  const [loading, setLoading] = useState<boolean>(false)
  const [error, setError] = useState<string | null>(null)
  const dispatch = useAppDispatch()
  const todos = useAppSelector((state: any) => state.task.value)

  useEffect(() => {
    setLoading(true)
    useCase
      .execute()
      .then((data) => {
        dispatch(setList(data))
      })
      .catch((err) => {
        setError(err.message)
        console.log(err)
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])

  return { todos, loading, error }
}
