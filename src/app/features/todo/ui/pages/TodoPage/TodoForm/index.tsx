import { TextField } from '@mui/material'
import { ChangeEvent, useState } from 'react'

type TodoFormProps = {
  defaultValue?: string
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void
  onSubmit: (value: string) => void
  label: string
}

function TodoForm(props: TodoFormProps) {
  const { defaultValue, onChange, onSubmit, label } = props
  const [value, setValue] = useState(defaultValue || '')

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
    if (onChange) {
      onChange(e)
    }
  }

  const handleSubmit = (e: ChangeEvent<HTMLFormElement>) => {
    e.preventDefault()
    onSubmit(value)
    setValue('')
  }

  return (
    <form onSubmit={handleSubmit} style={{ marginBottom: 20 }}>
      <TextField
        label={label}
        variant="outlined"
        onChange={handleChange}
        value={value}
        fullWidth
        size="small"
      />
    </form>
  )
}

export default TodoForm
