import { fireEvent, render, screen } from '@testing-library/react'
import TodoForm from '.'

describe('<TodoForm />', () => {
  let component = null
  const handleSubmitMock = jest.fn()
  const handleChangeMock = jest.fn()

  beforeEach(() => {
    component = render(
      <TodoForm
        onSubmit={handleSubmitMock}
        onChange={handleChangeMock}
        label="Add task"
      />
    )
    jest.clearAllMocks()
  })

  it('should create an instance', () => {
    expect(component).toBeDefined()
  })

  it('should validate that component is rendered correctly', () => {
    expect(screen.getByLabelText(/Add task/i)).toBeInTheDocument()
  })

  it('should change input value correctly', () => {
    const input = screen.getByLabelText('Add task')
    fireEvent.change(input, { target: { value: 'Test task' } })
    expect(screen.getByDisplayValue('Test task')).toBeInTheDocument()
  })

  it('should validate that onChange function that receive for Props works correctly', () => {
    const input = screen.getByLabelText('Add task')
    fireEvent.change(input, { target: { value: 'New task' } })
    expect(handleChangeMock).toHaveBeenCalled()
  })

  it('should validate that submit forms works correctly', () => {
    const input = screen.getByLabelText('Add task')
    fireEvent.change(input, { target: { value: 'New task' } })
    fireEvent.submit(input)
    expect(handleSubmitMock).toHaveBeenCalled()
    expect(handleSubmitMock).toBeCalledWith('New task')
  })
})
