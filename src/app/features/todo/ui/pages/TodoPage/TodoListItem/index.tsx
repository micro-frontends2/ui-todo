import { Card, Checkbox, IconButton } from '@mui/material'
import { useState } from 'react'
import deleteIcon from '../../../../../../core/assets/images/delete-icon.svg'

type TodoListItemProps = {
  taskId: number
  title: string
  defaultValue?: boolean
  onToggleState: (todoId: number) => void
  onDelete: (todoId: number) => void
}

function TodoListItem(props: TodoListItemProps) {
  const { taskId, title, defaultValue, onToggleState, onDelete } = props
  const [isChecked, setIsChecked] = useState<boolean>(
    defaultValue !== undefined ? defaultValue : false
  )

  const handleChange = () => {
    onToggleState(taskId)
    setIsChecked(!isChecked)
  }

  const handleDelete = () => {
    onDelete(taskId)
  }

  return (
    <Card
      elevation={4}
      style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: '0 10px',
        marginTop: 8,
        borderRadius: 8,
        minHeight: 60,
      }}
    >
      <Checkbox checked={isChecked} onChange={handleChange} />
      <div
        style={{
          width: '100%',
          fontFamily: 'Inter, "Helvetica Neue", Helvetica, Arial, sans-serif',
        }}
      >
        {title}
      </div>
      <IconButton
        onClick={handleDelete}
        role="btn-delete"
        aria-label="delete"
        size="medium"
      >
        <img src={deleteIcon} alt="delete" height="24px" />
      </IconButton>
    </Card>
  )
}

export default TodoListItem
