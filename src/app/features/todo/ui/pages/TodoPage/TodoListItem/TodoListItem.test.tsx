import { fireEvent, render, screen } from '@testing-library/react'
import TodoListItem from './index'

const implementedTests = ({
  component,
  handleDeleteMock,
  defaultValue = false,
}) => {
  // const { component, handleDeleteMock, defaultValue }: props

  it('should create an instance', () => {
    expect(component).toBeDefined()
  })

  it('should validate that component is rendered correctly', () => {
    expect(screen.getByText('Test task')).toBeInTheDocument()
  })

  it('should validate that status is change correctly (false -> true && true -> false)', () => {
    let el = screen.getByRole('checkbox', { checked: defaultValue })
    fireEvent.click(el)
    el = screen.getByRole('checkbox', { checked: !defaultValue })
    expect(el).toBeInTheDocument()

    fireEvent.click(el)
    el = screen.getByRole('checkbox', { checked: defaultValue })
    expect(el).toBeInTheDocument()
  })

  it('should validate that delete button is clickable', () => {
    const el = screen.getByRole('btn-delete')
    fireEvent.click(el)
    expect(handleDeleteMock).toHaveBeenCalledTimes(1)
  })
}

describe('<TodoListItem />', () => {
  let component = null
  const handleDeleteMock = jest.fn()
  const handleToggleStateMock = jest.fn()

  beforeEach(() => {
    component = render(
      <TodoListItem
        taskId={1}
        title="Test task"
        onToggleState={handleToggleStateMock}
        onDelete={handleDeleteMock}
      />
    )
    jest.clearAllMocks()
  })

  implementedTests({ component, handleDeleteMock })
})

describe('<TodoListItem /> with default value', () => {
  let component = null
  const handleDeleteMock = jest.fn()
  const handleToggleStateMock = jest.fn()
  const defaultValue = true

  beforeEach(() => {
    component = render(
      <TodoListItem
        taskId={1}
        title="Test task"
        defaultValue={defaultValue}
        onToggleState={handleToggleStateMock}
        onDelete={handleDeleteMock}
      />
    )
  })
  implementedTests({ component, handleDeleteMock, defaultValue })
})
