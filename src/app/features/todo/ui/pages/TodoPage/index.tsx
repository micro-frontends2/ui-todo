// @ts-ignore
import { Header } from '@demo/shared-module'
import container from '../../../../../../inversify.config'
import { AddTaskUseCase } from '../../../domain/usecases/addTask.usecase'
import { useAppDispatch } from '../../hooks/reduxHooks'
import { addTask } from '../../redux/taskSlice'
import TodoForm from './TodoForm'
import TodoList from './TodoList'

function TodoPage() {
  const addTaskUseCase = new AddTaskUseCase(container.get('ITaskRepository'))
  const dispatch = useAppDispatch()

  const handleSubmit = (inputValue: string) => {
    addTaskUseCase
      .execute(inputValue)
      .then((task) => {
        dispatch(addTask(task))
      })
      .catch((err) => {
        console.error(err)
      })
  }

  return (
    <>
      <Header>Todo Page</Header>
      <div style={{ padding: 20 }}>
        <TodoForm onSubmit={handleSubmit} label="New Task" />
        <TodoList />
      </div>
    </>
  )
}

export default TodoPage
