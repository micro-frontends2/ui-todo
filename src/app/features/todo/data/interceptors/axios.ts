import axios from 'axios'
import Router from '../../../../core/utils/router'

const instance = axios.create({
  baseURL: Router.baseApiUrl,
  timeout: 60000,
})

export default instance
