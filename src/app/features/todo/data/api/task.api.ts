import Router from '../../../../core/utils/router'
import { Task } from '../../domain/entities/task.entity'
import { fromJsonToListTaskAdapter } from '../adapters/fromJsonToListTask.adapter'
import axios from '../interceptors/axios'

class TaskApi {
  async findAll(): Promise<Task[]> {
    const jsonString = localStorage.getItem('tasks')

    if (jsonString) {
      return fromJsonToListTaskAdapter(JSON.parse(jsonString))
    }

    return axios
      .get(Router.tasks)
      .then((response) => {
        localStorage.setItem('tasks', JSON.stringify(response.data))
        return fromJsonToListTaskAdapter(response.data)
      })
      .catch((error) => {
        console.error(error)
        throw new Error('Task not found')
      })
  }
}

export default TaskApi
