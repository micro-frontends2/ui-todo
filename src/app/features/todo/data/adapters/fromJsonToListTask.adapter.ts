import { Task } from '../../domain/entities/task.entity'

export const fromJsonToListTaskAdapter = (jsonList: any[]): Task[] => {
  return jsonList.map((jsonTask: any) => {
    if (
      !jsonTask.id ||
      !jsonTask.title ||
      typeof jsonTask.completed !== 'boolean'
    ) {
      throw new Error('Invalid task')
    }
    return new Task(jsonTask.id, jsonTask.title, jsonTask.completed)
  })
}
