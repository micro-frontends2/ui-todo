import { fromJsonToListTaskAdapter } from './fromJsonToListTask.adapter'

describe('fromJsonToListTask', () => {
  const jsonList = [
    { id: '1', title: 'Task 1', completed: false },
    { id: '2', title: 'Task 2', completed: false },
    { id: '3', title: 'Task 3', completed: false },
  ]
  it('should validate array length', () => {
    const tasks = fromJsonToListTaskAdapter(jsonList)
    expect(tasks).toHaveLength(3)
  })

  it('should validate task properties', () => {
    const tasks = fromJsonToListTaskAdapter(jsonList)
    expect(tasks[0].id).toBe('1')
    expect(tasks[0].title).toBe('Task 1')
    expect(tasks[0].completed).toBe(false)
  })
  it('should throw error if task is invalid', () => {
    const invalidJsonList = [
      { id: '1', title: 'Task 1', completed: false },
      { id: '2', name: 'Task 2', completed: false },
      { id: '3', title: 'Task 3', completed: false },
      { id: '4', title: 'Task 4', completed: false },
    ]
    expect(() => {
      fromJsonToListTaskAdapter(invalidJsonList)
    }).toThrowError(/Invalid task/i)
  })
})
