import { injectable } from 'inversify'
import 'reflect-metadata'
import { Task } from '../../domain/entities/task.entity'
import { ITaskRepository } from '../../domain/repositories/task.repository'
import { fromJsonToListTaskAdapter } from '../adapters/fromJsonToListTask.adapter'
import TaskApi from '../api/task.api'

@injectable()
export class TaskRepositoryImpl implements ITaskRepository {
  private _taskService: TaskApi

  constructor() {
    this._taskService = new TaskApi()
  }

  public async getAll(): Promise<Task[]> {
    const tasks = await this._taskService.findAll()
    localStorage.setItem('tasks', JSON.stringify(tasks))
    return tasks
  }

  public add(task: Task): Promise<Task> {
    return new Promise((resolve) => {
      const json = JSON.parse(localStorage.getItem('tasks') ?? '[]')
      const tasks = fromJsonToListTaskAdapter(json)
      task.id = Math.max(...tasks.map((t) => t.id)) + 1
      tasks.push(task)
      localStorage.setItem('tasks', JSON.stringify(tasks))
      resolve(task)
    })
  }

  toggleState(taskId: number): Promise<void> {
    return new Promise((resolve) => {
      const json = JSON.parse(localStorage.getItem('tasks') ?? '[]')
      const tasks = fromJsonToListTaskAdapter(json)
      const newTasks = tasks.map((t) => {
        if (t.id === taskId) {
          t.completed = !t.completed
        }
        return t
      })
      localStorage.setItem('tasks', JSON.stringify(newTasks))
      resolve()
    })
  }

  public remove(taskId: number): Promise<void> {
    return new Promise((resolve) => {
      const json = JSON.parse(localStorage.getItem('tasks') ?? '[]')
      const tasks = fromJsonToListTaskAdapter(json)
      const task = tasks.find((t) => t.id === taskId)
      const taskIndex = tasks.indexOf(task)
      tasks.splice(taskIndex, 1)
      localStorage.setItem('tasks', JSON.stringify(tasks))
      resolve()
    })
  }
}
