const Router = {
  // routes for the app
  home: '/todo',

  // routes for the api
  baseApiUrl: 'https://jsonplaceholder.typicode.com',
  tasks: '/todos',
}

export default Router
