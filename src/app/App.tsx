import { Provider } from 'react-redux'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Router from './core/utils/router'
import TodoPage from './features/todo/ui/pages/TodoPage'
import { store } from './store'

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path={Router.home} element={<TodoPage />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  )
}

export default App
